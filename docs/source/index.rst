.. image:: ../../logo/logo_full_transparent.png
   :alt: logo

|

PyThia - Uncertainty Quantification Toolbox
===========================================

The PyThia UQ toolbox uses polynomial chaos surrogates to efficiently generate a surrogate of any (parametric) forward problem.
The surrogate is fast to evaluate, allows analytical differentiation and has a built-in global sensitivity analysis via Sobol indices.
Assembling the surrogate is done non-intrusive by least-squares regression, hence only training pairs of parameter realizations and evaluations of the forward problem are required to construct the surrogate.
No need to compute any nasty interfaces for lagacy code.

For more information, see the |PyThia GitLab repository|.

.. |PyThia GitLab repository| raw:: html

    <a href="https://gitlab.com/pythia-uq/pythia" target="_blank">PyThia GitLab repository</a>


Why the Name?
=============

Pythia was the title of the high priestess of the temple of Apollo in Delphi.
Hence you could say she used her prophetic abilities to quantify which was uncertain.
Moreover, the package is written in python, so...

How to cite PyThia
==================

There is no official related article to cite PyThia yet.
If you make use of PyThia in a publication, please use the meta data from the ``CITATION.cff`` file or cite it with a BibTeX entry similar to this:

::

    @software{pythia,
        author = {Hegemann, Nando and Heidenreich, Sebastian},
        title = {PyThia UQ Toolbox},
        version = {3.1.0},
        url = {https://pythia-uq.readthedocs.io/en/v3.1.0/},
        year = {2023},
        month = {4}
    }

License
=======

This work is dual-licensed under GNU Lesser General Public License v3.0 or later and Hippocratic License 3.0 or later.
You can choose between one of them if you use this work.

``SPDX-License-Identifier: LGPL-3.0-or-later OR Hippocratic-3.0-ECO-MEDIA-MIL``

Funding
=======

The development of PyThia UQ Toolbox vers. 1 and 2 has been funded by the German Central Innovation Program (ZIM) No. ZF4014017RR7.
The development of PyThia UQ Toolbox vers. 3 has been funded by the |EMPIR project 20IND04-ATMOC|.

.. |EMPIR project 20IND04-ATMOC| raw:: html

    <a href="https://www.atmoc.ptb.de/home/" target="_blank">EMPIR project 20IND04-ATMOC</a>

Logo
====

Access and usage information about the PyThia logo can be found under the following URL: |https://gitlab.com/pythia-uq/pythia-logo|.

.. |https://gitlab.com/pythia-uq/pythia-logo| raw:: html

    <a href="https://gitlab.com/pythia-uq/pythia-logo" target="_blank">https://gitlab.com/pythia-uq/pythia-logo</a>

Contents
========

.. toctree::
   :maxdepth: 1

   installation.rst
   tutorials/index.rst
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
