.. _tutorial-scat:

Tutorial - Shape Reconstruction via Scatterometry
=================================================

This tutorial shows how PyThia can be used for the shape reconstruction of a photolithography mask.

In this experiment a silicon line grating is irradiated by an electromagnetic wave under different angles of incidence :math:`\theta` and azimuthal orientations :math:`\phi`.
The aim of this experiment is to infer the six shape parameters :math:`x = (\mathit{h}, \mathit{cd}, \mathit{swa}, \mathit{t}, \mathit{r}_{\mathrm{top}}, \mathit{r}_{\mathrm{bot}})` from the detected scattered far field of the electromagnetic wave.
The experimental setup is visualized in the image below.

.. image:: ../../img/scat_setup.png
   :alt: scat-setup

Surrogate
---------

We start by loading the necessary packages.

.. code-block:: python

   import sys
   import numpy as np
   import pythia as pt
   import emcee

.. note::
   Generating independent samples of the Bayesian posterior is done here by Markov-Chain Monte Carlo sampling.
   Specifically, we rely on the |emcee| python package to do the sampling.

Generating training data from the forward problem requires solving Maxwell's equations with finite elements, which is very intricate and time consuming.
We assume the training data we rely on in this tutorial have been previously generated in an "offline" step, on a different machine using the |JCMsuite| software.
Hence we can simply load the data.

.. code-block:: python

   x_data = np.load("./x_train.npy")  # (10_000, 6)
   y_data = np.load("./y_train.npy")  # (90, 10_000, 2)
   w_data = np.load("./w_train.npy")  # (10_000, )

For the simulations we assumed Beta distributed parameters in reasonable parameter domains, which is also the density we drew the samples from.
In particular, this implies that the weights are simply :math:`w_i = 1 / N`, where :math:`N` is the number of samples (i.e., :math:`10.000` in this case).
The scattered intensity signal data consist of 4 curves (2 different azimuthal angles :math:`\phi` with s- and p- polarized wave signals each) where each curve is evaluated in 45 points (angles of incidence :math:`\theta`).
The continuous curves look similar to the image below.

.. image:: ../../img/scat_forward_eval.png
   :alt: scat-forward-eval

As a first step to compute a polynomial chaos expansion, we need to define the parameters using PyThias `Parameter` class.
Note that the parameters are given as a simple list.

.. code-block:: python
   
   h = pt.parameter.Parameter(1, 'h', [43, 53], 'beta', alpha=10.0, beta=9.67)
   cd = pt.parameter.Parameter(2, 'cd', [22, 28], 'beta', alpha=10.0, beta=7.75)
   swa = pt.parameter.Parameter(3, 'swa', [84, 90], 'beta', alpha=10.0, beta=10.12)
   t = pt.parameter.Parameter(4, 't', [4, 6], 'beta', alpha=10.0, beta=11.29)
   rtop = pt.parameter.Parameter(5, 'r_top', [8, 13], 'beta', alpha=10.0, beta=11.09)
   rbot = pt.parameter.Parameter(6, 'r_bot', [3, 7], 'beta', alpha=10.0, beta=12.34)
   params = [h, cd, swa, t, rtop, rbot]

Below you can find an image of the distribution of the loaded parameter samples.

.. image:: ../../img/scat_param_dist.png
   :alt: scat-param-dist

Next, we split the available data into a training and a test set, using roughly :math:`8.000` data points for the training.

.. code-block:: python
   
   split = int(0.8*x_data.shape[0])

   # training data
   x_train = x_data[:split]
   y_train = y_data[:split]
   w_train = w_data[:split]

   # test data
   x_test = x_data[split:]
   y_test = y_data[split:]

Now that we have our parameters defined and the training data in place, we only need to define which expansion terms we want to include into our expansion.
In this case, we choose a simplex index set to bound the total polynomial degree of the expansion by four.
This leaves us with :math:`210` polynomial chaos terms in our expansion.

.. code-block:: python
   
   indices = pt.index.simplex(dimension=len(params), maximum=4)
   index_set = pt.index.IndexSet(indices)

Finally, we have all the ingredients to compute our polynomial chaos surrogate, which requires only one line of code.

.. code-block:: python
   
   surrogate = pt.chaos.PolynomialChaos(params, index_set, x_train, w_train, y_train)

Evaluating the surrogate on our test set with

.. code-block:: python
   
   y_approx = surrogate.eval(x_test)

allows us compute an empirical approximation of the :math:`L^2`-error (MSE), which is approximately at :math:`5*10^{-3}`.
This is ok, since the measurement error of the observation we will investigate for the inverse problem will be at least one order of magnitude larger.

Sensitivity analysis
---------------------------
 
There are essentially two different types of sensitivity analyses, local and global.
Local sensitivity analysis typicalls considers a change in the signal by a small variation in the input around a specific point.
Given the polynomial chaos surrogate, computation of the local sensitivities is of course very easy as derivatives of the polynomial are given analytically.
To compute the local sensitivities of the second parameter (:math:`\mathit{cd}`) in all points of our test set, i.e. :math:`\partial/\partial x_{\mathit{cd}}\ f(x_{\mathrm{test}})`, we can use the optional ``partial`` argument of the ``eval()`` function

.. code-block:: python
   
   local_sensitivity = surrogate.eval(x_test, partial=[0, 1, 0, 0, 0, 0])

However, computing local sensitivities yields only a very limited image of the signal dependency on change in the individual parameter or combinations thereof.
When it comes to estimating whether the reconstruction of the hidden parameters will be possible it is often more reliable to consider the overall dependence of the signal on the parameters.
There are various types of global sensitivity analysis methods, but essentially all of them rely on integrating the target function over the high-dimensional parameter space, which is typically not feasible.
The polynomial chaos expansion, however, is closely connected to the so called Sobol indices, which allows us to compute these indices by squaring and summing different subsets of our expansion coefficients.
The Sobol coefficients are automatically computed with the polynomial chaos approximation and can be accessed via the ``surrogate.sobol`` attribute, which is ordered according to the list of Sobol tuples in ``index_set.sobol_tuples``.
As the signal data consist of multiple points on different curves, we get the Sobol indices for each point of our signal.
As this is hard to visualize however, we can have a look on the maximal value each Sobol index takes over the signal points.
The 10 most significant Sobol indices are listed in the table below.

.. csv-table:: Most relevant Sobol indices
   :header: "#", "Sobol tuple", "Parameter combinations", "Sobol index value (max)"
   :widths: auto

   "1",  ":math:`(2,)`",      ":math:`(\mathit{cd},)`",                                         ":math:`0.9853`"
   "2",  ":math:`(4,)`",      ":math:`(\mathit{t},)`",                                          ":math:`0.8082`"
   "3",  ":math:`(1,)`",      ":math:`(\mathit{h},)`",                                          ":math:`0.3906`"
   "4",  ":math:`(3,)`",      ":math:`(\mathit{swa},)`",                                        ":math:`0.2462`"
   "5",  ":math:`(2, 5)`",    ":math:`(\mathit{cd}, \mathit{r}_{\mathrm{top}})`",               ":math:`0.0332`"
   "6",  ":math:`(5,)`",      ":math:`(\mathit{r}_{\mathrm{top}},)`",                           ":math:`0.0240`"
   "7",  ":math:`(3, 5)`",    ":math:`(\mathit{swa}, \mathit{r}_{\mathrm{top}})`",              ":math:`0.0137`"
   "8",  ":math:`(1, 2)`",    ":math:`(\mathit{h}, \mathit{cd})`",                              ":math:`0.0065`"
   "9",  ":math:`(6,)`",      ":math:`(\mathit{r}_{\mathrm{bot}},)`",                           ":math:`0.0059`"
   "10", ":math:`(2, 3, 5)`", ":math:`(\mathit{cd}, \mathit{swa}, \mathit{r}_{\mathrm{top}})`", ":math:`0.0030`"

We see that all single parameter Sobol indices are among the top ten, implying that changes in single parameters are among the most influential.
Moreover, looking at the actual (maximal) values, we can assume that the first 4 parameters (:math:`\mathit{h}`, :math:`\mathit{cd}`, :math:`\mathit{swa}`, :math:`\mathit{t}`) can be reconstructed very well, whereas the sensitivities of the corner roundings :math:`\mathit{r}_{\mathrm{top}}` and :math:`\mathit{r}_{\mathrm{bot}}` imply that we will get large uncertainties in the reconstruction.

Inverse problem
---------------
 
For the inverse problem, i.e. the parameter reconstruction, we first need some measurements we want to infer the parameters for.
For academic purposes, we will consider an artificial measurement here for which we know the ground truth.
The easiest way to achieve this is adding some Gaussian noise to one of the test data, i.e.

.. code-block:: python
   
   x_true = x_test[-1].reshape(1, -1)
   y_true = y_test[-1].reshape(1, -1)
   std = 0.015
   noise = np.random.normal(0, std, y_true.shape)
   y_meas = y_true + noise

Note that we choose to add a relative noise of :math:`1.5\%` to the signal here.
As we typically don't know the signal to noise ratio of the measurement at hand, we introduce another parameter, :math:`b`, which we will reconstruct during the Markov-Chain Monte Carlo (MCMC) sampling as well.

.. code-block:: python
   
   hyper_params = [pt.parameter.Parameter(index=7, name="b", domain=[0, 0.1], distribution="uniform")]

As we can assume that the noise will not be more then :math:`10\%` of the signal, as the signal to noise ratio would us prevent from reconstruction then anyway, we can assume that the hyperparameter :math:`b` is uniformly distributed in the interval :math:`(0, 0.1)`.

Recall that the standard statistical regulatization of the inverse problem, assuming additive Gaussian noise, is done via Bayes theorem.
With this we get access to the parameter distribution via the Bayesian posterior

.. math::

   \pi_{x \vert y} = \frac{1}{Z} \pi_{y \vert x} \pi_x 
   \qquad\mbox{with}\qquad 
   \pi_{y \vert x}(y_{\mathrm{meas}} \vert x) = \frac{1}{(2\pi)^{6/2}\sqrt{\det \Sigma}} \exp\Bigl( -\frac{1}{2} \Vert y_{\mathrm{meas}} - f(x) \Vert_{\Sigma^{-1}} \Bigr),

where :math:`Z` is a normalization constant (evidence), :math:`\Sigma` is the covariance matrix and :math:`\pi_{y \vert x}` and :math:`\pi_x` are the likelihood and prior density, respectively.
In general it is not clear how to choose the covariance matrix :math:`\Sigma` (which is typically known as "error model").
For our purpose, however, considering and easy error model such as :math:`\Sigma = \operatorname{diag}(b\, y_{\mathrm{meas}})^2` suffices.

Due to the nature of the MCMC sampling algorithm, it suffices to know the posterior :math:`\pi_{x \vert y}` up to a constant, hence we can ignore the hard to compute evidence :math:`Z`.
Adding the hyperparameter :math:`b` to the original formulation lets us define the extended prior and thus posterior density

.. code-block:: python
   
   forward_model = lambda x: surrogate.eval(x[:,:-1])  # extended forward model
   sigma = lambda x: np.abs(x[:, -1]).reshape(-1, 1)*np.ones((1, y_meas.shape[1]))  # error model

   prior = pt.sampler.ParameterSampler(params+hyper_params)  # extended prior
   lh = Gaussian(forward_model, sigma, len(params+hyperparams))  # define Gaussian likelihood
   def log_posterior(x, meas):
       return lh.log_likelihood(x, meas) + prior.log_pdf(x)

.. note::
   We define the log-posterior as the sampling scheme becomes much more stable this way.
   Additionally, as the evidence is independent of all the parameters, it gets clear why we can ignore it here.

What remains is to specify the number of parallel MCMC runs and the sampling length as well as using |emcee| for the actual sampling.

.. code-block:: python
   
   n_walkers = 15
   chain_length = 10_000
   seed = prior.sample(n_walkers)
   runner = emcee.EnsembleSampler(n_walkers, 7, log_posterior, args=[y_meas])

   state = runner.run_mcmc(seed, 500, progress=True)
   runner.reset()  # discard burn-in
   runner.run_mcmc(seed, chain_length, progress=True)

After the MCMC sampler is done, we can access the samples and compare how well we were able to reconstruct the ground truth values.

.. code-block:: python
   
   samples = runner.get_chain(flat=True)
   mean = np.mean(samples, axis=0)
   std = np.std(samples, axis=0)

The result, depending on the length of the MCMC chains, should look something like this.

.. image:: ../../img/scat_mcmc_post.png
   :alt: scat-mcmc-post

As we can see, the ground truth for all 6 parameters as well as the hyperparameter :math:`b` lies well within the support of the sampled distribution.
Further we see that our sensitivity analysis has predicted the correct results, the posterior distribution for :math:`\mathit{cd}` and :math:`\mathit{t}` are the most concentrated ones, whereas the support of the posterior for :math:`\mathit{r}_{\mathrm{top}}` and :math:`\mathit{r}_{\mathrm{bot}}` is the largest.
Also, we can see that this procedure gives us a fairly accurate estimate for the measurement noise added to the ground truth data given the large original interval :math:`(0, 0.1)`.
Finally, the figure below shows how well the actual reconstruction really fits the signal.
Solid lines represent the ground truth signal, the dots represent the noisy measurements and the dashed lines are the result of our surrogate evaluated in the mean values of our posterior samples.

.. image:: ../../img/scat_result.png
   :alt: scat-result

This concludes the tutorial.

.. |emcee| raw:: html

    <a href="https://emcee.readthedocs.io/en/stable/index.html" target="_blank">emcee</a>

.. |JCMsuite| raw:: html

    <a href="https://www.docs.jcmwave.com/JCMsuite/html/index.html" target="_blank">JCMsuite</a>


